const state = {
    connectionMap: {},
    schemasMap: {}
}

const mutations = {
    SET_CONNECTION: (state, connection) => {
        state.connectionMap[connection.connectionId] = connection
    },
    SET_SCHEMAS: (state, data) => {
        console.log(data)
        state.schemasMap[data.connectionId] = data.schemas
    }
}

const actions = {
    setConnection({ commit }, connection) {
        return new Promise(resolve => {
            commit('SET_CONNECTION', connection)
            resolve(connection)
        })
    },
    setSchemas({ commit }, data) {
        return new Promise(resolve => {
            commit('SET_SCHEMAS', data)
            resolve(data)
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
