const mysql = require('mysql')

// 创建MySQL连接
export function createConnection (params) {
  return mysql.createConnection({
    host: params.host,
    prot: params.port,
    user: params.user,
    password: params.password,
    database: params.database || ''
  })
}

// 连接
export function connect (connection) {
  connection.connect(function (err) {
    if (err) {
      console.error(err.code)
      console.error(err.fatal)
    }
  })
}

// 查询
export function query (connection, sql) {
  return new Promise(function (resolve, reject) {
    if (!connection) {
      return
    }
    connection.query(sql, [], function selectCb (err, rows, fields) {
      if (err) {
        console.error('An error ocurred performing the query.')
        console.error(err)
        reject(err)
        return
      }
      resolve({
        err: err,
        rows: rows,
        fields: fields
      })
    })
  })
}

// 关闭连接
export function closeConnection (connection) {
  connection.end(function () {
    // The connection has been closed
  })
}
