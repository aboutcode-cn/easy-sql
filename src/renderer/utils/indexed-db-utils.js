import Dexie from 'dexie'

export class IndexedDB extends Dexie {
    constructor() {
        // run the super constructor Dexie(databaseName) to create the IndexedDB database.
        super('indexed-db')

        this.version(1).stores({
            mysqlConnection: '++id,connectionName,host,port,username,pwd,rememberMe',
            mysqlQuery: '++id,connectionId,databaseName,queryName,querySql'
        })

        this.mysqlConnection = this.table('mysqlConnection')
        this.mysqlQuery = this.table('mysqlQuery')
    }

    async listConnection(connectionName) {
        let connections
        if (connectionName) {
            connections = await this.mysqlConnection.where('connectionName').equals(connectionName).toArray()
        } else {
            connections = await this.mysqlConnection.orderBy('id').toArray()
        }
        return connections
    }
    updateConnection(connection) {
        return this.mysqlConnection.update(connection.id, connection)
    }
    addConnection(connection) {
        return this.mysqlConnection.add(connection)
    }
    deleteConnection(id) {
        return this.mysqlConnection.delete(id)
    }
    async listQuery() {
        let queryList = await this.mysqlQuery.orderBy('id').toArray()
        return queryList
    }
    updateQuery(query) {
        return this.mysqlQuery.update(query.id, query)
    }
    addQuery(query) {
        return this.mysqlQuery.add(query)
    }
    deleteQuery(id) {
        return this.mysqlQuery.delete(id)
    }
}
