# easySQL

easySQL是一款跨平台的开源MySQL客户端。你可以在easySQL上进行表结构设计（DDL），也可以进行表数据操作（DML）DML。easySQL专门针对SQL查询做了优化设计，以互操作的方式教你如何编写高质量的SQL，特别适合学习数据库知识的初学者。
![数据表管理](https://image.codeiy.com/data-table-manage.png "数据表管理")

easySQL支持三大主流操作系统：Windows，Linux和MacOS。下载对应平台上的最新的release版本进行安装，安装完就可以开始愉快的玩耍了。


easySQL采用Electron技术，基于vue框架构建跨平台桌面应用程序。